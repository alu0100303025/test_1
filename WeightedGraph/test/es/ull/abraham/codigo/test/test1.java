package es.ull.abraham.codigo.test;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import es.ull.abraham.codigo.*;


public class test1 {
	public WeightedGraph t;
	@Before
	public void antesDelTest() {
		/**
		 * El metodo precedido por la etiqueta @Before
		 * es para indicar a JUnit que debe ejecutarlo
		 * antes de ejecutar los Tests que figuran en
		 * esta clase.
		 * Aqui cargamos un grafo de prueba
		 */
		this.t = new WeightedGraph (6);
		t.setLabel (0, "v0");
		t.setLabel (1, "v1");
		t.setLabel (2, "v2");
		t.setLabel (3, "v3");
		t.setLabel (4, "v4");
		t.setLabel (5, "v5");
       	t.addEdge (0,1,2);
       	t.addEdge (0,5,9);
       	t.addEdge (1,2,8);
       	t.addEdge (1,3,15);
       	t.addEdge (1,5,6);
       	t.addEdge (2,3,1);
       	t.addEdge (4,3,3);
       	t.addEdge (4,2,7);
       	t.addEdge (5,4,3);
	}
	
	@Test
	/* Comprobamos que la carga ha sido realizada correctamente
	 */
	public void testCarga() {
       	String arr_txt_carga[] = {"v0: v1:2 v5:9 ","v1: v2:8 v3:15 v5:6 ","v2: v3:1 ","v3: ","v4: v2:7 v3:3 ","v5: v4:3 "};
       	String grafo[]= this.t.print_array();
       	boolean correcto = true;
       	for (int i=0; i<6; i++) {
       		correcto &= (grafo[i].equals(arr_txt_carga[i]));
       	}
       	assertTrue(correcto);
	}

	@Test
	/*Comprobamos que al hacer el Dijsktra los valores de los recorridos sean correctos
	con los datos de prueba */
	public void testDijsktra() {
		final int [] pred = Dijkstra.dijkstra (this.t, 0);
       	boolean correcto = true;
       	String arr_txt_dijkstra[] = {"[v0]","[v0, v1]","[v0, v1, v2]","[v0, v1, v2, v3]","[v0, v1, v5, v4]","[v0, v1, v5]"};
       	for (int n=0; n<6; n++) {  		
       		correcto &= (arr_txt_dijkstra[n].equals(Dijkstra.printPath_str (this.t, pred, 0, n)));
       	}
       	assertTrue(correcto);       	
	}
	
	@Test
	/*Comprobamos que al hacer el Dijsktra los predecesores sean correctos
	con los datos de prueba */
	public void testPredecesores() {
		final int [] pred = Dijkstra.dijkstra (this.t, 0);
       	boolean correcto = true;
       	int arr_pred[] = {0,0,1,2,5,1};
       	for (int n=0; n<6; n++) {
       		correcto &= (arr_pred[n] == pred[n]);
       	}
       	assertTrue(correcto);       	
	}	
}
